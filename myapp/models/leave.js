const _ = require('underscore');
const DATA_DATE_FORMAT = 'DD-MMM-YYYY';
const allEmps = require('../data/employee').employee;
const im = require('../data/bizType').im;
const ce = require('../data/bizType').ce;
const m = require('moment');
const INPUT_DATE_FORMAT = 'YYYYMMDD';

const splitEmpBasedOnEligibility = (eDate) => {
    const endDate = m(eDate, INPUT_DATE_FORMAT);    
    const partitionToEligibleAndNonEligible = _.groupBy(allEmps, x => m(x.joinDate,DATA_DATE_FORMAT).isSameOrBefore(endDate));
    return [partitionToEligibleAndNonEligible.true || [], partitionToEligibleAndNonEligible.false || []];
}

const handleNonEligibileEmp = (nonEligibleEmp) => {
    return nonEligibleEmp.map(x => {
        return {...x, slcl:'NA', pl:'NA', total: 'NA'};
    });
}

const handleEligibleEmp = (eligibleEmp, endDate) => {
    return eligibleEmp.map(emp => emp.bizType === 'IM' ? handleIMEmp(emp, endDate) : handleCEEmp(emp, endDate));
}

const handleIMEmp = (emp, eDate) => {    
    const qData = getIMEligiblieQuarter(emp.joinDate);
    const fullQuarters = getFullQuarters(eDate, emp.joinDate);
    const result = {...emp, slcl: qData.slcl + fullQuarters * 3, pl: qData.pl + fullQuarters * 4};
    result.total = result.slcl + result.pl;
    return result;
}

const getIMEligiblieQuarter = (joinDate) => {
    const jd = m(joinDate, DATA_DATE_FORMAT);   
    const eligibleQuarter = im.filter(q => q.quarter === jd.quarter()).filter(ed => {
        const year = jd.format('YYYY');
        return m(`${ed.from}-${year}`,DATA_DATE_FORMAT).isSameOrBefore(jd) && m(`${ed.to}-${year}`,DATA_DATE_FORMAT).isSameOrAfter(jd);
    });
    if(eligibleQuarter.length > 0){
        return eligibleQuarter[0];
    }
    return {slcl:0, pl:0};
}

const getCEEligiblieQuarter = (joinDate) => {
    const jd = m(joinDate, DATA_DATE_FORMAT);   
    console.log('JD', jd, jd.date());
    const eligibleQuarter = ce.filter(d => jd.date() >= d.from  && jd.date() <= d.to);
    if(eligibleQuarter.length > 0){
        return eligibleQuarter[0];
    }
    return {slcl:0, pl:0};
}

const getFullQuarters = (eDate, joinDate) => {
    const jd = m(joinDate, DATA_DATE_FORMAT);  
    const endDate = m(eDate, INPUT_DATE_FORMAT);    
    return endDate.endOf('quarter').diff(jd.endOf('quarter'),'months')/3;
}

const getMonths = (eDate, joinDate) => {
    const jd = m(joinDate, DATA_DATE_FORMAT);  
    const endDate = m(eDate, INPUT_DATE_FORMAT);    
    //console.log(endDate.endOf('month'),jd.endOf('month'));
    return endDate.endOf('month').diff(jd.endOf('month'),'months');
}

const handleCEEmp = (emp, endDate) => {
     
    const qData = getCEEligiblieQuarter(emp.joinDate);
    const fullQuarters = getMonths(endDate, emp.joinDate);    
    let result = {...emp, slcl: qData.slcl + fullQuarters * 1, pl: qData.pl + fullQuarters * 1.25};
    result.total = result.slcl + result.pl;
    return result;
}

module.exports.splitEmpBasedOnEligibility = splitEmpBasedOnEligibility;
module.exports.handleNonEligibileEmp = handleNonEligibileEmp;
module.exports.handleEligibleEmp = handleEligibleEmp;

//console.log(m('12-Dec-2021', DATA_DATE_FORMAT).endOf('month'), m('03-Oct-2021', DATA_DATE_FORMAT).endOf('month').diff(m('01-Feb-2021',DATA_DATE_FORMAT).endOf('month'),'months'));